# Description

- `Geyer_2010_full.scone`: original Geyer controller from scone with
  fix for allow_neg_L = 0
- `Geyer_2010_full_augmented.scone`: added Ia spindle and other reflexes with
  gains = 0 so that we can generate signals for imitation learning using the
  healthy gait model
- `Geyer_2010_pelvis_tilt.scone`: only pelvis PD controller that regulates torso
  tilting (actually pelvis because torso is one with the pelvis)

Deprecated:

- `Geyer_2010_no_ankle_muscles.scone`: same as before without ankle muscle
  reflexes
