# Description

- `gait_measure.scone`: gait measure used to obtain healthy gait with Geyer's
  controller (normal gait speed, Wang effort, and joint limits)
- `gait_measure_one_segment.scone`: slightly different parameters used during
  optimization of the segmental controller
- `gait_measure_mimic.scone`: combination of the `gait_measure.scone` and a
  mimic measure over the `healthy_gait.sto`; used for segmental controller
  (ankle)
- `gait_measure_mimic_unstable.scone`: combination of mimic and StepMeasure that
  encourages to stay alive and increase distance travel; used when model falls
  immediately so as to gain reward if start to move forward
- `gait_measure_multiobjective.scone`: a custom StepMeasure for self-selected,
  low, or high speed with death penalty, effort and joint limits, appropriate
  for multi-objective optimization; used for healthy multi-objective
  optimziation
- `gait_measure_multiobjective_unstable.scone`: same as above without death
  penalty; used for unstable (not robust) multi-objective optimziation
