# Segmental controller

In this implementation, it is assumed that the spinal cord is composed of
multiple spinal cord segments (e.g., L1, L2, S2, etc.). Each spinal cord segment
contains different types of neurons. Here we assume sensory, inter and motor
neuron types. A muscle transmits sensory information to the spinal cord through
afferents that can innervates multiple segments. Also, a muscle receives neural
excitation from more than one spinal cord segments. The organization of the
spinal cord segments is defined by two things: 1) the muscle organization
specifications, and 2) reflex rules. The muscle organization specifies muscle
dependent information such as antagonist or synergist muscles and the
corresponding spinal cord segments. The reflex rules define the type of
mono/polysynaptic pathways that exist in the spinal cord. Both of these
specifications are defined by the user of this controller. The controller
translates this into a network of interconnected neurons, which is used to
control the muscles in SCONE.

## Reflex rules

In order to define the topological network the user must provide the
`ReflexRules` and `SpinalCordOrganization`. The syntax for a `ReflexRule` is:

```
ReflexRule {
	source_role = [agonist|antagonist|extensor|synergist]
	source_neuron = [SN_Ia|SN_II|SN_Ib|IN_Ia ...|IN_RC|FF|MN]
	action = [excite|ingibit|excite_or_inhibit]
	destination_role = [agonist|antagonist|extensor|synergist]
	destination_neuron = [SN_Ia|SN_II|SN_Ib|IN_Ia ...|IN_RC|MN]
}
```

The rule looks at the `SpinalCordOrganization` and applies the rule based on the
properties of each muscle (e.g., agonists, antagonists, segment, extensor,
etc.). In the above example a source neuron can have different tags (agonist,
antagonist) so that we can say "agonist `SN_Ia` inhibits antagonist
`IN_Ia`". Here, we use the prefix `SN_` or `IN_` to distinguish the neurons
based on their type (sensor, inter, motor neurons). With the exception of `MN`
which does not have any prefix and denotes a motor neuron type. For now, we
support only the following sensor neurons `SN_Ia`, `SN_Ib`, and `SN_II`. `FF` is
a special type of neuron (please check `Neuron models and parameterization`
below).

*Example (stretch reflex):*

```
ReflexRules {
	ReflexRule {
		source_role = agonist
		source_neuron = SN_Ia
		action = excite
		destination_role = agonist
		destination_neuron = MN
	}
	ReflexRule {
		source_role = agonist
		source_neuron = SN_Ia
		action = excite
		destination_role = agonist
		destination_neuron = IN_Ia
	}
	ReflexRule {
		source_role = agonist
		source_neuron = IN_Ia
		action = inhibit
		destination_role = antagonist
		destination_neuron = IN_Ia
	}
	ReflexRule {
		source_role = agonist
		source_neuron = IN_Ia
		action = inhibit
		destination_role = antagonist
		destination_neuron = MN
	}
}
```

*Example (extensor Ib facilitation):*

```
ReflexRules {
	ReflexRule {
		source_role = extensor
		source_neuron = SN_Ib
		action = excite
		destination_role = extensor
		destination_neuron = IN_Iab
	}
	ReflexRule {
		source_role = extensor
		source_neuron = IN_Iab
		action = excite
		destination_role = extensor
		destination_neuron = MN
	}
}
```

*Example (Renshaw cells):*

```
ReflexRules {
	ReflexRule {
		source_role = agonist
		source_neuron = MN
		action = excite
		destination_role = agonist
		destination_neuron = IN_RC
	}
	ReflexRule {
		source_role = agonist
		source_neuron = IN_RC
		action = inhibit
		destination_role = agonist
		destination_neuron = MN
	}
	ReflexRule {
		source_role = agonist
		source_neuron = IN_RC
		action = inhibit
		destination_role = antagonist
		destination_neuron = IN_RC
	}
	ReflexRule {
		source_role = agonist
		source_neuron = IN_RC
		action = inhibit
		destination_role = antagonist
		destination_neuron = IN_Ia
	}
}
```

## Spinal cord organization

For a `ReflexRule` to be applied it must know more about how muscles are
organized. The syntax for is as follows:

```
Organization {
	agonist = muscle_name
	synergists = [muscle_name_1 muscle_name_2 ...]
	antagonists = [muscle_name_1 muscle_name_2 ...]
	segments = [segment_name_1 segment_name_2 ...]
	extensor = [true|false]
	excluded_neurons = [neuron_type_1 neuron_type_2 ...]
}
```

Here, a muscle can have zero or many synergies and antagonists. A muscle can be
part of one or more spinal cord segments (mean of organization). A muscle may or
may not be an extensor muscle. We can choose to exclude specific types of
neurons for a particular muscle (e.g., no `SN_II`). *Currently, we do not handle
synergy rules, but this is reserved for future use.**

*Example (ankle flexor-extensor):*

```
SpinalCordOrganization {
	Organization {
		agonist = gastroc
		antagonists = [tib_ant]
		segments = [L5] # S1 S2
		extensor = true
	}
	Organization {
		agonist = soleus
		antagonists = [tib_ant]
		segments = [L5] # L5 S1 S2
		extensor = true
	}
	Organization {
		agonist = tib_ant
		antagonists = [soleus gastroc]
		segments = [L5] # L4 L5 S1
	}
}

```

If we choose to utilize more segments, then the number of neurons and parameters
increases by a lot and obtaining a stable gait is challenging.

## Pattern formation layer

We provide a mechanism to excite motor neurons using a pattern formation
layer. One can define different patterns using Bezier curves. These curves are
parameterized by a variable 0 <= phi <= 1. Each pattern is connected to all
motor neurons. Symmetry is assumed between left and right leg muscles to reduce
the number of parameters (connection weights).

```
Patterns {
	Bezier {
		control_points = 5
		control_point_y = 0.5~0.5<0,2>
	}
	Bezier {
		control_points = 5
		Y2 = 2
	}
	Bezier {
		control_points = 5
		Y3 = 0.5~0.5<0,2>
	}
}
pattern_muscle_weight = 0.01~0.1<0,1>

```

The above example creates three patterns of order 4 (e.g., 5 control points). In
the first example, all control points are decision variables that are to be
determined through optimization. The second pattern defines a curve where $Y0 =
Y1 = Y3 = Y4 = 0$, therefore the control points are not optimized. The third
pattern assumes $Y0 = Y1 = Y2 = Y4 = 0$, where $Y3$ is the only decision
variable. Finally, the pattern-to-muscle weights are also decision variables.

In this implementation, the patterns are parameterized using the phase variable
`phi`. We use two phase oscillators (left and right legs) that are reset based
on foot contact events as in Aoi et al. 2010. The phase oscillator must be
defined inside the `OpenSimModel3` component so that its differential equations
can be numerically solved along with the rest of the states. Error checks are
performed internally and the user is properly notified.

```
ModelOpenSim3 {

	...

	# used by pattern_formation_neurons_t in SegmentalController
	PhaseOscillator {
		omega = 5~0.1<0,10>
	}
}

```

## Direct connection of neurons

We have also implemented a facility to directly connect two neurons
using the following syntax:


```
DirectConnections {
	Connection {
		source_muscle = muscle_name
		source_segment = segment_name
		source_neuron_type = neuron_type
		action = [excite|ingibit|excite_or_inhibit]
		destination_muscle = muscle_name
		destination_segment = segment_name
		destination_neuron_type = neuron_type
	}
}

```

*Example: connect P0_r.L5.PF with gastroc_r.L5.IN_Ia*

```
DirectConnections {
	Connection {
		source_muscle = P0_r
		source_segment = L5
		source_neuron_type = PF
		action = excite_or_inhibit
		destination_muscle = gastroc_r
		destination_segment = L5
		destination_neuron_type = IN_Ia
	}
}

```

## Neuron models and parameterization

### Base neuron

The neurons implement a leaky-integrator that performs an Euler integration step
(internally) each time the output is required. This implementation ensures that
if one requests the output of a particular neuron, then all input neurons are
also advanced in time. In other words, if one requires the output of a motor
neuron, that depends on the output of other neurons, then the output of these
neurons are automatically calculated and cached properly. The implementation is
defined below:

tau * dV / dt = -V + I

y = f(V)

where, V is the membrane potential, I is the input current, tau the time
constant, y the output of the neuron and f the activation function. The input
current I is calculated as follows:

I = w\_0 + \sum\_j w\_j * y\_i

where, w\_0 [-inf, inf] is the bias term, w\_j the presynaptic control [-inf,
inf], and y\_j is the outputs of the neurons that connect to this neuron.

We can control the parameterization of the neurons using the commands

```
bias = 0.0~0.1<-2,2> # for w0
tau = 0.01
excitatory_synapse = 0.5~0.5<0,2>
inhibitory_synapse = -0.5~0.5<-2,0>
```

### Sensor neuron

Sensor neuron type. Gets muscles related sensory information from SCONE. For
now, only muscle fiber length, velocity, and force sensors are supported. The
input of the sensor uses the following convention:

I = w0 + s * g(t - dt)

where, s is the sensor scaling factor and g(t - dt) is the delayed value
of the afferent. In our case, g can be either normalized fiber length,
velocity or muscle force. The output of the neuron is governed by the
differential equation of the base neuron (leaky-integrator).

We can control the parameterization of the neurons using the commands

```
# SN sensory neurons
sensor_scale = 1.0 # for s
# sensor_scale = 1.0~0.1<0,2>

# II neurons
length_offset = -0.8~0.5<-1.5,0> # w0 for II

# Ia neurons
tau_Ia = 0.01
use_prochazka = true
velocity_offset = 0.0 # w0 for Ia

# Ib neurons
force_offset = 0.0 # w0 for Ib

```

### Feed-forward state dependent neuron (experimental)

Feed-forward state-dependent input neuron. The input of this neuron is defined
by:

I = w0 + wp[phi]

where, wp contains the weight for a specific phase determined by phi (0 <= phi
<= 4). This works only in combination with SCONE's `GaitStateController`.

```
# feed-forward neurons
feed_forward_offset = 0
default_phase_weight = 0.1~0.1<0,0.4>

# reflex rules
ReflexRules {
	# feed-forward neurons
	ReflexRule {
		source_role = agonist
		source_neuron = FF
		action = excite
		destination_role = agonist
		destination_neuron = MN
	}
	...

```

## Other parameters

To generate the topological graph (useful for debugging) set `generate_graph =
true` in `SegmentalController`. It outputs `graphviz` (.gv) files which one can
convert to `.ps` and inspect.

```
#!/bin/bash

dot -Tps L4.gv -o L4.ps
dot -Tps L5.gv -o L5.ps
dot -Tps S1.gv -o S1.ps
dot -Tps S2.gv -o S2.ps

```

Neuron communication delays are determined by `sensory_delay_file =
delay/neural_delay_testing.txt` in `SegmentalController`.
