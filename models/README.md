# Description

- `gait0914_original.osim`: model from SCONE
- `gait0914.osim`: gastroc max isometric force increased to 4690 N
- `gait0914_rigid_tendon.osim`: same as above with rigid tendon

Deprecated:

- `gait0914_back.osim`: same as `gait0914.osim` model but with back
  joint and a coordinate actuator
- `gait0914_thelen.osim`: same as `gait0914` but with Thelen2003Muscle
- `gait0914_downhill_straight_uphill.osim`: variable terrain (experimental)
- `gait0918.osim`: model from Ong with the knee limit forces from SCONE
- `gait0918_back.osim`: same as `gait0918.osim` model but with back
  joint and a coordinate actuator
