# Description

Healthy gait:

- `initial_state.sto`: initial states used for healthy gait

- `healthy_gait.sto`: simulation of Geyer's controller used as reference for
  training more complex controllers with gastroc max_isometric_force set to 4690
  N
- `healthy_gait_original.sto`: simulation of Geyer's controller used as reference for
  training more complex controllers with original gait0914 from SCONE  
- `healthy_gait_scone.par`: one of the best solutions obtained by the SCONE's
  initial controller, which however had some issues with non-negative iliopsoas
  II feedback
- `healthy_gait_terrain.sto`: optimization of Geyer's controller on slopes

Segmental controller (one segment):

- `spinal_cord_one_segment.sto`: solution of segmental controller with one
  segment
- `spinal_cord_one_segment_original.sto`: solution using the original gait0914
  model model
- `spinal_cord_one_segment_terrain.sto`: solution with slopes

Segmental controller (all segment):

- `spinal_cord_all_segments.sto`: first good optimization of the multi-segmental
  spinal controller

Deprecated:

- `healthy_gait_back.sto`: good solution for `healthy_gait_back.scone`
- `healthy_gait_ong.sto`: simulation of Ong's model
- `healthy_gait_ong_back.sto`: simulation of Ong's model with back joint
