# Description

SCONE scenario scripts containing experiments carried using the segmental
level model.

## Healthy gait

- `healthy_gait.scone`: optimized healthy gait using Geyer's
  controller with changes to the gait0914 model
- `healthy_gait_original.scone`: optimized healthy gait using Geyer's
  controller using original gait0914 model from SCONE
- `healthy_gait_multiobjective.scone`: healthy gait using multi-objective
  optimization to obtain different combinations of gait speed vs effort

## Healthy experimental

- `healthy_gait_pattern_imitate.scone`: optimize only for feed-forward pattern
  formation using imitation
- `healthy_gait_pattern_only.scone`: optimize only for feed-forward pattern
  formation using parameters obtained from imitation (previous)

## Healthy deprecated:

- `healthy_gait_ong.scone`: optimized healthy gait using Geyer's
  controller and Ong's model
- `healthy_gait_ong_back.scone`: optimized healthy gait using Geyer's
  controller and Ong's model with back joint
- `healthy_gait_back.scone`: optimized healthy gait using Geyer's
  controller from SCONE with back joint
- `healthy_gait_thelen.scone`: optimized healthy gait using Geyer's
  controller from SCONE with Thelen's muscle model instead of Millard

## Segmental controller ankle (deprecated)

- `spinal_cord_ankle.scone`: stable solution of a segmental controller on the
  muscles spanning the ankle joint
- `spinal_cord_ankle_multiobjective.scone`: stable solution of a segmental
  controller on the muscles spanning the ankle joint

## Segmental controller full (one segment)

- `sipnal_cord_one_segment.scone`: first stable version of the segmental
  controller and pattern formation for one segment
- `sipnal_cord_one_multiobjective.scone`: same as above but for multi-objective
  optimization
- `sipnal_cord_one_terrain.scone`: a single segmental controller with uneven
  terrain to test if CPG modulation could help

## Segmental controller full (multi-segment)

- `spinal_cord_all_segments_imitate.scone`: imitate healthy gait to initialize
  the network parameter for the multi-segmental controller
- `spinal_cord_all_segments.scone`: use the initial solution of the imitation to
  further optimize the multi-segmental controller

Further instructions are provided in sub-folders (e.g., `segmental_controller`).
