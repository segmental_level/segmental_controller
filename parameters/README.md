# Description

Healthy gait:

- `healthy_gait.par`: parameters of original Geyer's controller that
  give good gait with gastroc max_isometric_force set to 4690 N
- `healthy_gait_original.par`: parameters of original Geyer's controller that
  give good gait using gait0914 model from SCONE
- `healthy_gait_terrain.par`: optimization of Geyer's controller on slopes

Segmental controller (one segment)

- `spinal_cord_one_segment_imitate.par`: solution obtained by imitating the
  healthy_gait
- `spinal_cord_one_segment.par`: solution for one segment after optimizing
  using initial conditions from imitate
- `spinal_cord_one_segment_original.par`: solution for one segment after
  optimizing using initial conditions from imitate but with the original
  gait0914 model
- `spinal_cord_one_segment_terrain.par`: optimization of one segment model on
  slopes

Segmental controller (all segment)

- `spinal_cord_all_segments_imitate.par`: solution obtained by imitating the
  healthy_gait
- `spinal_cord_all_segments.par`: solution for all segments model after
  optimizing using initial conditions from imitate (not good yet due to changes)

Deprecated:

- `healthy_gait_back.par`: parameters of original Geyer's controller
  but with a PD controller for the back joint (which is present in the model)
- `healthy_gait_ong.par`: parameters optimized for the Ong's model
- `healthy_gait_ong_back.par`: Ong's model with back join
- `spinal_cord_ankle.par` parameters for the ankle segmental controller

