# Description

- `evaluate_parameters.sh`: script for evaluating results parameters
- `generate_graph.sh`: script for generating segmental controller topological
  graphs (should be placed and called within SCONE/bin)
- `multi_objective.py` script for analyzing multi-objective solutions obtained
  from pagmo2 in SCONE
- `sync.sh`: example command for syncing remote result folder locally (used when
  optimizing on the cluster)
- `visualize_kinematics_kinetics.py`: utility script for analyzing .sto files
  from SCONE
- `visualize_neuron_activity.py`: utility script for analyzing neuron activities
  of the segmental level model
