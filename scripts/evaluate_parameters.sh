#!/usr/bin/env bash

# first argument $1 is the relative or absolute path to the simulation
# folder containing the .par files
results_dir=$(realpath $1)

# please change SCONE's install dir accordingly
scone_install=/shared-dev/stanev/scone/bin

# parallel evaluation
# find $results_dir -type f -name "*par" | while read file_name; do "$scone_install"/sconecmd -e "$file_name";  done
# find $results_dir -type f -name "*par" | while read file_name; do "$scone_install"/sconecmd -e "$file_name" &  done
find $results_dir -type f -name "*par" | parallel -j$(nproc) "$scone_install"/sconecmd -e {}