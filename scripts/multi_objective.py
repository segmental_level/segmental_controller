# Visualize the Pareto front of SCONE's multi-objective optimization
#
# Dimitar Stanev (dimitar.stanev@epfl.ch)
# %%
from os import listdir
from os.path import join, isdir, splitext
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import numpy as np
plt.switch_backend('TKAgg')
import pygmo

# %%
# methods


def get_solutions_per_evolution(working_dir):
    temp = {}
    for top in listdir(working_dir):
        if isdir(join(working_dir, top)) and top.isdigit():
            files_in_dir = listdir(join(working_dir, top))
            key = int(top)
            temp[key] = []
            for item in files_in_dir:
                file_name, extension = splitext(item)
                if extension == '.par' \
                   and not file_name == 'healthy_gait' \
                   and file_name.find('inf') == -1:
                    obj_values = file_name.split('_')[1].split('-')[1:-1]
                    temp[key].append([float(value) for value in obj_values])

            if len(temp[key]) == 0:
                temp.pop(key, None)

    return temp


def plot_solutions(solutions, is_3d=True, comp=[0, 1, 2],
                   x_label='speed penalty',
                   y_label='effort penalty',
                   z_label='joint limit penalty'):
    cmap = cm.jet
    vmax = len(solutions) + 1

    # figure
    fig = plt.figure()
    ax = None
    if is_3d:
        ax = fig.add_subplot(111, projection='3d')
    else:
        ax = fig.add_subplot(111)

    all_solutions = None
    for key in sorted(solutions.keys()):

        # get objectives
        values = solutions[key]
        if len(values) == 0:
            continue

        # collect data to calculate non-dominated solutions
        data = np.array(values)
        if all_solutions is None:
            all_solutions = data
        else:
            all_solutions = np.concatenate((all_solutions, data), axis=0)

        # plot solutions
        if is_3d:
            ax.scatter(data[:, comp[0]], data[:, comp[1]], data[:, comp[2]],
                       c=data.shape[0] * [int(key)],
                       cmap=cmap,
                       vmin=0, vmax=vmax,
                       label=key)
            ax.set_zlabel(z_label)
        else:
            ax.scatter(data[:, comp[0]], data[:, comp[1]],
                       c=data.shape[0] * [int(key)],
                       cmap=cmap,
                       vmin=0, vmax=vmax,
                       label=key)

        ax.set_xlabel(x_label)
        ax.set_ylabel(y_label)

    # visualize the non-dominated solutions
    front, _, _, _ = pygmo.fast_non_dominated_sorting(all_solutions)
    if is_3d:
        im = ax.scatter(all_solutions[front[0], comp[0]],
                        all_solutions[front[0], comp[1]],
                        all_solutions[front[0], comp[2]],
                        marker='+',
                        c=len(front[0]) * [int(key + 1)],
                        cmap=cmap,
                        vmin=0, vmax=vmax,
                        label='ND')
    else:
        im = ax.scatter(all_solutions[front[0], comp[0]],
                        all_solutions[front[0], comp[1]],
                        marker='+',
                        c=len(front[0]) * [int(key + 1)],
                        cmap=cmap,
                        vmin=0, vmax=vmax,
                        label='ND')

    # fig.legend()
    plt.colorbar(im, ax=ax)
    plt.tight_layout()
    plt.show(block=False)

# %%
# Uchida

working_dir = './multiobjective/self_selected_uchida'
solutions = get_solutions_per_evolution(working_dir)
plot_solutions(solutions, is_3d=False, comp=[1, 2],
               x_label='effort penalty',
               y_label='joint limit penalty')

# %%
# Wang long

working_dir = './multiobjective/self_selected_wang_long'
solutions = get_solutions_per_evolution(working_dir)
plot_solutions(solutions, is_3d=False, comp=[1, 2],
               x_label='effort penalty',
               y_label='joint limit penalty')

# %%
# loads

working_dir = './multiobjective/self_selected_wang_loads'
solutions = get_solutions_per_evolution(working_dir)
plot_solutions(solutions, comp=[1, 2, 3],
               x_label='effort',
               y_label='limit',
               z_label='loads')

# %%
# MOEAD

working_dir = './multiobjective/self_selected_wang_moead'
solutions = get_solutions_per_evolution(working_dir)
plot_solutions(solutions, is_3d=False, comp=[1, 2],
               x_label='effort penalty',
               y_label='joint limit penalty')

# %%
# Wang short

working_dir = './multiobjective/self_selected_wang_short'
solutions = get_solutions_per_evolution(working_dir)
plot_solutions(solutions, is_3d=False, comp=[1, 2],
               x_label='effort penalty',
               y_label='joint limit penalty')

# %%
# slow Wang

working_dir = './multiobjective/slow_wang'
solutions = get_solutions_per_evolution(working_dir)
plot_solutions(solutions)

# %%
# fast Wang

# working_dir = './multiobjective/fast_wang'
# solutions = get_solutions_per_evolution(working_dir)
# plot_solutions(solutions)

# %%
