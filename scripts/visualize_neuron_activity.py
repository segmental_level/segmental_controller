# Brief Visualize neuron activities.
#
# author: Dimitar Stanev <dimitar.stanev@epfl.ch>
# %%
import os
import numpy as np
import matplotlib
import pandas as pd
matplotlib.rcParams.update({'font.size': 12})
matplotlib.rcParams.update({'legend.framealpha': 0.2})
import matplotlib.pyplot as plt
from utils import read_from_storage
from utils import plot_pattern_formation_weights
from utils import plot_neuron_activity
from utils import plot_neuron_activity_grid
from utils import plot_neuron_activity_3D
from utils import plot_segment_chrod

# %%

# load state
# state_file = os.path.abspath('../states/spinal_cord_all_segments.sto')
state_file = os.path.abspath('../states/spinal_cord_one_segment.sto')
# state_file = os.path.abspath('../states/step_length/small_steplength.sto')
# state_file = os.path.abspath('../states/healthy_gait.sto')
# state_file = os.path.abspath('../states/healthy_gait_back.sto')
# state_file = os.path.abspath('../states/healthy_gait_ong.sto')
# state_file = os.path.abspath('../states/healthy_gait_ong_back.sto')
state = read_from_storage(state_file)

# %%

# load parameters
# parameter_file = os.path.abspath('../parameters/spinal_cord_all_segments.par')
parameter_file = os.path.abspath('../parameters/spinal_cord_one_segment.par')
parameters = pd.read_csv(parameter_file, delimiter='\s+', header=None)
parameters.dropna(axis=1, inplace=True)
parameters.columns = ['parameter', 'best', 'mean', 'std']
parameters.set_index('parameter', inplace=True)

# %%
# plot configurations

muscles = ['iliopsoas', 'glut_max', 'hamstrings', 'vasti', 'tib_ant',
           'gastroc', 'soleus']
muscle = 'soleus_r'
neuron = 'MN'
segment = 'L5'
cycle = 4
side = 'r'
col = 4

# muscles = ['iliopsoas', 'glut_max', 'hamstrings', 'bifemsh', 'vasti',
#            'rect_fem', 'tib_ant', 'gastroc', 'soleus']
# col = 3

# %%
# plot pattern formation weights

plot_pattern_formation_weights(parameters, muscles, parameter_file)

# %%
# plot neuron activity

plot_neuron_activity(state, muscle, segment, neuron, cycle, state_file)
plot_neuron_activity_grid(state, muscle, segment, neuron, cycle, state_file)
plot_neuron_activity_3D(state, muscle, segment, neuron, cycle, state_file)

# %%
# plot segment connectivity distribution

plot_segment_chrod(state, segment, side, 'matplotlib', state_file)
plot_segment_chrod(state, segment, side, 'bokeh', state_file)

# %%

import holoviews as hv

muscle = 'tib_ant_r'
segment = 'L5'

# select columns
regex = '{muscle}.{segment}.*->{muscle}.{segment}.*'\
    .format(muscle=muscle, segment=segment)
mask = state.columns.str.contains(regex)
df = state.iloc[:, mask]

# transform data
tran_df = pd.DataFrame(columns=['source', 'destination', 'strength'])
for column in df.columns:
    split = column.split('->')
    source = split[0]
    destination = split[1]
    strength = np.abs(df[column].median())
    tran_df = tran_df.append({'source': source, 'destination': destination,
                              'strength': strength}, ignore_index=True)

# normalize strength
max_lines = 50
min_lines = 10
min_s = tran_df.strength.min()
max_s = tran_df.strength.max()
tran_df.strength = (tran_df.strength - min_s) / (max_s - min_s) * max_lines
tran_df.strength = tran_df.strength.astype(int)
tran_df.strength = tran_df.strength + min_lines
# tran_df.sort_values(by=['strength'], ascending=True, inplace=True)

# labels (it is important to sort them)
neurons = list(set(tran_df['source'].unique().tolist() + \
                   tran_df['destination'].unique().tolist()))
neurons.sort()
neurons_dataset = hv.Dataset(pd.DataFrame(neurons, columns=['neuron']))

# construct chord plot
hv.extension("bokeh")
# hv.extension('matplotlib')
hv.output(size=300)
chord = hv.Chord((tran_df, neurons_dataset), ['source', 'destination'],
                 ['strength'])
chord.opts(title=segment, labels='neuron',
           node_color='neuron', node_cmap='Category20',
           edge_color='source', edge_cmap='Category20',
           edge_alpha=1.0)
hv.save(chord, 'chord.html')
# hv.save(chord, 'chord.pdf')
# from bokeh.plotting import figure, show
# show(hv.render(chord))
# show(chord, 'firefox')

# Select the 20 busiest airports
# strong = list(tran_df.sort_values('strength').iloc[3:].index.values)
# strongest = chord.select(index=strong, selection_mode='nodes')

# hv.save(chord, 'chrod.pdf')
# hv.save(strongest, 'chrod2.pdf')

# fig = hv.render(chord)
# p = hv.render(chord, backend='bokeh')

# from IPython.display import display_html
# html = renderer.html(chord)
# display_html(html, raw=True)


# %%
