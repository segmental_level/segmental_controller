# Brief Visualize kinematics, kinematics, and muscle activity of a SCONE
# simulation.
#
# author: Dimitar Stanev <dimitar.stanev@epfl.ch>
# %%
import os
from utils import read_from_storage
from utils import perform_muscle_analysis
from utils import plot_scone_kinematics
from utils import plot_scone_vertical_reactions
from utils import plot_scone_joint_moments
from utils import plot_scone_muscle_excitations


# %%
# settings

model_file = os.path.abspath('../models/gait0914.osim')
state_file = os.path.abspath('../states/healthy_gait.sto')
state = read_from_storage(state_file)
muscle_analysis_output_dir = os.path.abspath('./muscle_analysis/')

side = 'r'
muscles = ['iliopsoas', 'glut_max', 'hamstrings', 'vasti', 'tib_ant',
           'gastroc', 'soleus']
columns = 4

# muscles = ['iliopsoas', 'glut_max', 'hamstrings', 'bifemsh', 'vasti',
#            'rect_fem', 'tib_ant', 'gastroc', 'soleus']
# col = 3


# %%
# perform muscle analysis to calculate muscle induced moments

# run this once
perform_muscle_analysis(model_file, state_file, muscle_analysis_output_dir)

# %%
# plot joint moments

plot_scone_kinematics(state, side, state_file)
plot_scone_vertical_reactions(state, side, state_file)
plot_scone_joint_moments(state, model_file, muscle_analysis_output_dir, side,
                         state_file)
plot_scone_muscle_excitations(state, muscles, side, columns, state_file)

# %%
