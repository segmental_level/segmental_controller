SegmentalController {
	# generate graph of the network for inspection
	generate_graph = true
	
	# neuron delay
	sensory_delay_file = delay/neural_delay_testing.txt
	
	# baseclass neuron
	tau = 0.01
	# bias = 0.0~0.1<-2,2>
	bias = 0
	excitatory_synapse = 0.5~0.5<0,2>
	inhibitory_synapse = -0.5~0.5<-2,0>
	
	# SN sensory neurons
	sensor_scale = 1.0
	# sensor_scale = 1.0~0.1<0,2>
	
	# II neurons
	length_offset = -0.8~0.5<-1.5,0>
	
	# Ia neurons
	tau_Ia = 0.01
	use_prochazka = true
	velocity_offset = 0.0
	
	# Ib neurons
	force_offset = 0.0
	
	# feed-forward neurons
	#feed_forward_offset = 0
	#default_phase_weight = 0.1~0.1<0,0.4>
	
	# reflex rules
	ReflexRules {
		# feed-forward neurons
		#ReflexRule {source_role = agonist source_neuron = FF action = excite destination_role = agonist destination_neuron = MN}
		
		# stretch reflex
		ReflexRule {source_role = agonist source_neuron = SN_Ia action = excite destination_role = agonist destination_neuron = MN}
		ReflexRule {source_role = agonist source_neuron = SN_Ia action = excite destination_role = agonist destination_neuron = IN_Ia}
		ReflexRule {source_role = agonist source_neuron = IN_Ia action = inhibit destination_role = antagonist destination_neuron = IN_Ia}
		ReflexRule {source_role = agonist source_neuron = IN_Ia action = inhibit destination_role = antagonist destination_neuron = MN}
		
		# inhibitory Ib reflex
		ReflexRule {source_role = agonist source_neuron = SN_Ib action = excite destination_role = agonist destination_neuron = IN_Ib}
		ReflexRule {source_role = agonist source_neuron = IN_Ib action = inhibit destination_role = agonist destination_neuron = MN}
		ReflexRule {source_role = agonist source_neuron = IN_Ib action = inhibit destination_role = antagonist destination_neuron = IN_Ib}
		
		# extensor Ib facilitation
		ReflexRule {source_role = extensor source_neuron = SN_Ib action = excite destination_role = extensor destination_neuron = IN_Iab}
		ReflexRule {source_role = extensor source_neuron = IN_Iab action = excite destination_role = extensor destination_neuron = MN}
		# must fix a bug in antagonists who are not extensors (orphan neurons)
		# ReflexRule {source_role = extensor source_neuron = IN_Iab action = inhibit destination_role = antagonist destination_neuron = IN_Iab}
		
		# II facilitation
		ReflexRule {source_role = agonist source_neuron = SN_II action = excite destination_role = agonist destination_neuron = IN_IaII}
		ReflexRule {source_role = agonist source_neuron = IN_IaII action = excite destination_role = agonist destination_neuron = MN}
		ReflexRule {source_role = agonist source_neuron = IN_IaII action = inhibit destination_role = antagonist destination_neuron = MN}
		
		# Renshaw cells
		ReflexRule {source_role = agonist source_neuron = MN action = excite destination_role = agonist destination_neuron = IN_RC}
		ReflexRule {source_role = agonist source_neuron = IN_RC action = inhibit destination_role = agonist destination_neuron = MN}
		ReflexRule {source_role = agonist source_neuron = IN_RC action = inhibit destination_role = antagonist destination_neuron = IN_RC}
		ReflexRule {source_role = agonist source_neuron = IN_RC action = inhibit destination_role = antagonist destination_neuron = IN_Ia}
	}
	
	# muscle organization
	SpinalCordOrganization {
		Organization {
			agonist = gastroc
			#synergists = [soleus]
			antagonists = [tib_ant]
			segments = [L5] # S1 S2
			extensor = true
		}
		Organization {
			agonist = soleus
			#synergists = [gastroc]
			antagonists = [tib_ant]
			segments = [L5] # L5 S1 S2
			extensor = true
		}
		Organization {
			agonist = tib_ant
			antagonists = [soleus gastroc]
			segments = [L5] # L4 L5 S1
			# excluded_neurons = [IN_Iab]
		}
	}
	
	# pattern formation feed-forward controller
	Patterns{	
		RaisedCosine {
			mean = 0.1#~0.1<0,0.2>
			half_width = 0.2#~0.2<0.1,0.3>
		}
		RaisedCosine {
			mean = 0.3#~0.3<0.2,0.4>
			half_width = 0.2#~0.2<0.1,0.3>
		}
		RaisedCosine {
			mean = 0.5#~0.5<0.4,0.6>
			half_width = 0.2#~0.2<0.1,0.3>
		}
		RaisedCosine {
			mean = 0.7#~0.7<0.6,0.8>
			half_width = 0.2#~0.2<0.1,0.3>
		}
		RaisedCosine {
			mean = 0.9#~0.9<0.8,1.0>
			half_width = 0.2#~0.2<0.1,0.3>
		}
	}
	pattern_muscle_weight = 0.01~0.1<0,1>
}